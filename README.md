## jQuery Atlassian plugin 

It is a P2 maven plugin which wraps [jQuery][jq], and optionally [jQuery migrate plugin][jqm],
designed for the cross-product consumption.

## Branches

- `master` - jQuery 3.x
- `2.x` - jQuery 2.x

## Usage

This project provides a few `web-resource` that you can depend upon in yours.

Be aware that your product might already depend on jQuery through this plugin.
Your product may configure and expose jQuery via a product-specific web-resource or module.
If so, you should use your product's `web-resource` keys instead of these ones.

| `web-resource` key | [jQuery library][jq] | [jQuery migrate plugin][jqm] | [`jQuery.browser`][jqb] |
|------------------|------------|----------------|
|`com.atlassian.plugins.jquery:jquery`| ✅ | ✅ | ✅ |
|`com.atlassian.plugins.jquery:jquery-lib`| ✅ | ❌ | ❌ |
|`com.atlassian.plugins.jquery:jquery-browser`| ✅ | ❌ | ✅ |
|`com.atlassian.plugins.jquery:jquery-migrate`| ✅ | ✅ | ✅ |

By default, [jQuery migrate][jqm] will not log any warnings to console. If you want those,
you will need to enable the `com.atlassian.plugins.jquery:jquery-migrate-logging` web-resource
via UPM.

## Issues

Issues should be raised on [the JQUERY ecosystem project][5].

## Contributing

Make sure that generated files, including minified versions (`-min.js`),
are not empty and contain license comments.

The license for new versions should be added in atlassian-licenses repository
for [jQuery itself][4] and [this maven plugin][3] accordingly.


[jq]: http://jquery.com
[jqm]: https://github.com/jquery/jquery-migrate
[jqb]: https://github.com/gabceb/jquery-browser-plugin
[3]: https://stash.atlassian.com/projects/BUILDENG/repos/atlassian-licenses/browse/com/atlassian/plugins/jquery
[4]: https://stash.atlassian.com/projects/BUILDENG/repos/atlassian-licenses/browse/other/jqueryjs
[5]: https://ecosystem.atlassian.net/browse/JQUERY
